'''setup.py'''
from setuptools import setup

import hbp_nrp_cle

reqs_file = './requirements.txt'
install_reqs = list(val.strip() for val in open(reqs_file))
reqs = install_reqs

config = {
    'description': 'Python Implementation of Closed Loop Engine',
    'author': 'HBP Neurorobotics',
    'url': 'http://neurorobotics.net',
    'author_email': 'neurorobotics@humanbrainproject.eu',
    'version': hbp_nrp_cle.__version__,
    'install_requires': reqs,
    'packages': ['hbp_nrp_cle',
                 'hbp_nrp_cle.brainsim',
                 'hbp_nrp_cle.brainsim.pynn',
                 'hbp_nrp_cle.brainsim.pynn.devices',
                 'hbp_nrp_cle.brainsim.common',
                 'hbp_nrp_cle.brainsim.common.devices',
                 'hbp_nrp_cle.brainsim.pynn_nest',
                 'hbp_nrp_cle.brainsim.pynn_nest.devices',
                 'hbp_nrp_cle.brainsim.pynn_spiNNaker',
                 'hbp_nrp_cle.brainsim.pynn_spiNNaker.devices',
                 'hbp_nrp_cle.brainsim.nest',
                 'hbp_nrp_cle.brainsim.nest.devices',
                 'hbp_nrp_cle.brainsim.nengo',
                 'hbp_nrp_cle.brainsim.nengo.devices',
                 'hbp_nrp_cle.cle',
                 'hbp_nrp_cle.mocks',
                 'hbp_nrp_cle.mocks.brainsim',
                 'hbp_nrp_cle.mocks.brainsim.__devices',
                 'hbp_nrp_cle.mocks.cle',
                 'hbp_nrp_cle.mocks.robotsim',
                 'hbp_nrp_cle.tf_framework',
                 'hbp_nrp_cle.tf_framework.spike_generators',
                 'hbp_nrp_cle.robotsim'],
    'package_data': {
        'hbp_nrp_cle': ['config.ini']
    },
    'classifiers': ['Programming Language :: Python :: 3'],
    'scripts': [],
    'name': 'hbp-nrp-cle',
    'include_package_data': True,
}

setup(**config)
