.. _cle-developer-manual:

Closed Loop Engine developer space
==================================

The CLE code is written in Python 3.8 and it's base module is :class:`hbp_nrp_cle`. Here you can find the :ref:`Python API documentation <cle-api>` as well as the detailed description of the :ref:`software architecture<cle-architecture-index>`.

.. toctree::
    :maxdepth: 2

    architecture/index
    .. developer_manual/index
    python_api