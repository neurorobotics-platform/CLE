#!/bin/bash
set -e
set -o
set -x

# Debug printing
whoami
env | sort

if [ -z "${HBP}" ]; then
    echo "USAGE: The HBP variable not specified"
    exit 1
fi

USER_SCRIPTS_DIR=${USER_SCRIPTS_DIR:-"user-scripts"}
EXP_CONTROL_DIR=${EXP_CONTROL_DIR:-"ExperimentControl"}
CLE_DIR=${CLE_DIR:-"CLE"}
BRAIN_SIMULATION_DIR=${BRAIN_SIMULATION_DIR:-"BrainSimulation"}
EXDBACKEND_DIR=${EXDBACKEND_DIR:-"ExDBackend"}

export PYTHONPATH=

source "${HBP}/${USER_SCRIPTS_DIR}/nrp_variables"

cd "${HBP}/${CLE_DIR}"

# Checkout config.ini.sample from user-scripts

cp "${HBP}/${USER_SCRIPTS_DIR}/config_files/CLE/config.ini.sample" hbp_nrp_cle/hbp_nrp_cle/config.ini

# Concatenate all build requirements, ensure newline in between
(echo; cat "${HBP}/${EXP_CONTROL_DIR}/hbp_nrp_excontrol/requirements.txt") >> hbp_nrp_cle/requirements.txt

# Configure build

export NRP_INSTALL_MODE=dev
export IGNORE_LINT='platform_venv|hbp_nrp_cle/hbp_nrp_cle/bibi_config/generated|hbp_nrp_cle/.eggs|nest'
export MPLBACKEND=agg

# python-tk is needed by matplotlib, which is needed by csa, which is required by spynnaker8
# MPLBACKEND tells matplotlib not to use python-tk, so we can install spynnaker later without having python-tk


# verify_base-ci fails on dependencies mismatch, but ignores linter errors, which are cought by Jenkins afterwards
rm -rf build_env
virtualenv build_env \
    && . build_env/bin/activate \
    && make --always-make verify_base-ci
